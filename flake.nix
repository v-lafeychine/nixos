{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";

    home-manager = {
      url = "github:nix-community/home-manager/master";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nixpkgs-master.url = "github:NixOS/nixpkgs/master";

    # emacs-overlay = {
    #   url = "github:nix-community/emacs-overlay/master";
    #   inputs.nixpkgs.follows = "nixpkgs";
    # };

    # coq-lsp = { 
    #   type = "git"; 
    #   url = "https://github.com/ejgallego/coq-lsp.git";
    #   submodules = true;
    # };
  };

  outputs = { self, nixpkgs, ... }@inputs:
    let
      lib = nixpkgs.lib.extend (_: prev: {
        my = import ./lib.nix {
          inherit inputs;
          lib = prev;
        };
      });

      system = "x86_64-linux";
    in {
      packages = lib.my.mkForAllArch
        (_: pkgs: lib.my.mapModules ./packages (p: pkgs.callPackage p { }));

      nixosConfigurations =
        lib.my.mapHosts ./hosts { inherit inputs lib system; };
    };
}

