{ config, inputs, lib, pkgs, options, ... }:

with lib.my;

{
  imports = [ inputs.home-manager.nixosModules.home-manager ]
    ++ (mapModulesRec (toString ./modules) import);

  options = {
    hm = mkOpt (lib.types.attrs) { };
    user = mkOpt (lib.types.attrs) { };
  };

  config = {
    home-manager.users.lafeyc_v = lib.mkAliasDefinitions options.hm;
    users.users.lafeyc_v = lib.mkAliasDefinitions options.user;

    environment.systemPackages = with pkgs; [ exfat ntfs3g sshfs git ];

    hm.home.stateVersion = config.system.stateVersion;

    nix = let
      filteredInputs = lib.filterAttrs (n: _: n != "self") inputs;
      registryInputs = lib.mapAttrs (_: v: { flake = v; }) filteredInputs;
    in {
      package = pkgs.nixUnstable;

      registry = registryInputs // { dotfiles.flake = inputs.self; };

      settings = {
        experimental-features = [ "nix-command" "flakes" ];
        substituters = [ "https://nix-community.cachix.org/" ];
        trusted-public-keys = [
          "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
        ];
      };
    };

    nixpkgs.config.allowUnfree = true;

    time.timeZone = "Europe/Paris";

    services.fwupd.enable = true;

    programs.adb.enable = true;
    services.udev.packages = [ pkgs.android-udev-rules ];

    user = {
      createHome = true;
      description = "Vincent Lafeychine";
      extraGroups = [ "adbusers" "libvirtd" "wheel" ];
      home = "/home/lafeyc_v";
      isNormalUser = true;
      hashedPassword =
        "$y$j9T$FbIWL95YYU69MXwTzckZf1$zKTreivH6ayrWT7TQi7IgrXNKGJX4r4ToH9nPzEdUS4";
    };

    users.mutableUsers = false;
    users.users.root.hashedPassword = "*";
  };
}
