{ config, lib, inputs, pkgs, ... }:

{
  imports = [ inputs.nixos-hardware.nixosModules.lenovo-thinkpad-t470s ];

  boot.initrd.availableKernelModules =
    [ "nvme" "sd_mod" "usb_storage" "xhci_pci" ];

  boot.kernelModules = [ "iwlwifi" "kvm-intel" ];
  boot.kernelPackages = pkgs.linuxPackages_latest;

  boot.loader.systemd-boot.enable = true;

  hardware.enableAllFirmware = true;

  networking.useDHCP = lib.mkDefault true;

  system.stateVersion = "21.11";

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/aa5c9b83-da28-4f7e-a9d3-fa7de455ceab";
      fsType = "ext4";
    };

    "/boot" = {
      device = "/dev/disk/by-uuid/D009-E9DA";
      fsType = "vfat";
    };

    "/nix/store" = {
      device = "/nix/store";
      fsType = "none";
      options = [ "bind" ];
    };
  };

  swapDevices =
    [{ device = "/dev/disk/by-uuid/b85ddfa6-55e3-4dab-93da-5098a7715dd6"; }];
}
