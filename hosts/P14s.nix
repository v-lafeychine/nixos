{ config, inputs, lib, modulesPath, pkgs, ... }:

let
  nvidia-offload = pkgs.writeShellScriptBin "nvidia-offload" ''
    export __NV_PRIME_RENDER_OFFLOAD=1
    export __NV_PRIME_RENDER_OFFLOAD_PROVIDER=NVIDIA-G0
    export __GLX_VENDOR_LIBRARY_NAME=nvidia
    export __VK_LAYER_NV_optimus=NVIDIA_only
    exec "$@"
  '';

  think-status = pkgs.writeShellScriptBin "think-status" ''
    watch -n .5 \
      'cat /proc/cpuinfo | grep "MHz" && echo && \
       sensors | grep "Core" && echo && \
       cat /proc/acpi/ibm/fan | head -n 3 | tail -n 2 && echo && \
       cat /sys/devices/virtual/powercap/intel-rapl-mmio/intel-rapl-mmio:0/constraint_0_power_limit_uw && echo && \
       cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor && echo && \
       nvidia-smi'
  '';
in {
  imports = with inputs.nixos-hardware.nixosModules; [
    common-cpu-intel-cpu-only
    common-pc-laptop-acpi_call
    lenovo-thinkpad
  ];

  boot.initrd.availableKernelModules =
    [ "nvme" "sd_mod" "thunderbolt" "usb_storage" "xhci_pci" ];
  boot.initrd.kernelModules = [ "dm-snapshot" ];

  boot.kernelParams = [ "intel_pstate=passive" ];

  boot.kernelModules = [ "coretemp" "kvm-intel" ];
  boot.kernelPackages = pkgs.linuxPackages_latest;

  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.systemd-boot.enable = true;

  environment.systemPackages = [ nvidia-offload think-status ];

  hardware = {
    enableAllFirmware = true;

    opengl = {
      enable = true;
      # mesaPackage = pkgs.mesa;
    };

    nvidia = {
      package = config.boot.kernelPackages.nvidiaPackages.beta;

      prime = {
        offload.enable = true;

        intelBusId = "PCI:0:2:0";
        nvidiaBusId = "PCI:1:0:0";
      };
    };
  };

  networking.useDHCP = lib.mkDefault true;

  powerManagement.cpuFreqGovernor = "ondemand";

  # services.thinkfan.enable = true;
  # services.throttled.enable = true;
  services.xserver.videoDrivers = [ "nvidia" ];

  system.stateVersion = "22.05";

  boot.initrd.luks.devices = {
    root = {
      device = "/dev/disk/by-uuid/839ed436-271f-468f-9f43-6632a68f3c3b";
      preLVM = true;
    };
  };

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/6aeeb268-4d0d-4d00-acdf-33abcd0212b5";
    fsType = "ext4";
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/40AC-6891";
    fsType = "vfat";
  };

  swapDevices =
    [{ device = "/dev/disk/by-uuid/a8b06aec-fef5-4970-8b51-9624f3032550"; }];
}
