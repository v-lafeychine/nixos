# Functions from https://github.com/hlissner/dotfiles

{ inputs, lib, ... }:

with builtins;
with lib;

rec {
  mapFilterAttrs = pred: f: attrs: filterAttrs pred (mapAttrs' f attrs);

  mkOpt = type: default: mkOption { inherit type default; };

  mapModules = dir: fn:
    mapFilterAttrs (n: v: v != null && !(hasPrefix "_" n)) (n: v:
      let path = "${toString dir}/${n}";
      in if v == "directory" && pathExists "${path}/default.nix" then
        nameValuePair n (fn path)
      else if v == "regular" && n != "default.nix" && hasSuffix ".nix" n then
        nameValuePair (removeSuffix ".nix" n) (fn path)
      else
        nameValuePair "" null) (readDir dir);

  mapModulesRec = dir: fn:
    let
      dirs = mapAttrsToList (k: _: "${dir}/${k}")
        (filterAttrs (n: v: v == "directory" && !(hasPrefix "_" n))
          (readDir dir));
      files = attrValues (mapModules dir id);
      paths = files ++ concatLists (map (d: mapModulesRec d id) dirs);
    in map fn paths;

  mkHost = path:
    attrs@{ inputs, lib, system, ... }:
    nixosSystem {
      inherit system;
      specialArgs = { inherit inputs lib; };
      modules = [
        {
          networking.hostName =
            mkDefault (removeSuffix ".nix" (baseNameOf path));
        }
        ./configuration.nix
        path
      ];
    };

  genAttrs' = values: f: listToAttrs (map f values);

  mkForAllArch = f:
    genAttrs' lib.systems.flakeExposed (system:
      let nixpkgs = inputs.nixpkgs.legacyPackages."${system}";
      in {
        name = system;
        value = f system nixpkgs;
      });

  mapHosts = dir: attrs: mapModules dir (hostPath: mkHost hostPath attrs);
}
