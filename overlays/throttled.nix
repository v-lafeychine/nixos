final: prev: {
  throttled = prev.throttled.overrideAttrs (_: {
    src = final.fetchFromGitHub {
      owner = "erpalma";
      repo = "throttled";
      rev = "1dd726672f0b11b813d4c7b63e0157becde7a013";
      sha256 = "sha256-0MsPp6y4r/uZB2SplKV+SAiJoxIs2jgOQmQoQQ2ZKwI=";
    };
  });
}

