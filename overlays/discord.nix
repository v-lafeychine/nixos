final: prev: {
  discord = prev.discord.override { 
    nss = final.nss_latest; 
    withOpenASAR = true;
  };
}
