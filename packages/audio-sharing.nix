{ lib
, stdenv
, fetchFromGitLab
, git
, appstream-glib
, cargo
, glib
, desktop-file-utils
, gst_all_1
, gtk4
, meson
, ninja
, pkg-config
, rustPlatform
, rustc
, libadwaita
, dbus
}:

stdenv.mkDerivation rec {
  pname = "audio-sharing";
  version = "0.2.2";

  src = fetchFromGitLab {
    domain = "gitlab.gnome.org";
    owner = "World";
    repo = "AudioSharing";
    rev = version;
    sha256 = "sha256-ejNktgN9tfi4TzWDQJnESGcBkpvLVH34sukTFCBfo3U=";
  };

  cargoDeps = rustPlatform.fetchCargoTarball {
    inherit src;
    name = "${pname}-${version}";
    sha256 = "sha256-c19DxHF4HFN0qTqC2CNzwko79uVeLeyrrXAvuyxeiOQ=";
  };

  postPatch = ''
    patchShebangs build-aux
  '';

  nativeBuildInputs = [
    appstream-glib
    desktop-file-utils
    meson
    ninja
    pkg-config
    rustPlatform.cargoSetupHook
    cargo
    rustc
    git
  ];

  buildInputs = [
    dbus
    glib
    gtk4
    gst_all_1.gst-rtsp-server
    gst_all_1.gst-plugins-base
    gst_all_1.gst-plugins-good
    gst_all_1.gst-plugins-bad
    gst_all_1.gst-plugins-ugly
    gst_all_1.gst-libav
    gst_all_1.gstreamer
    libadwaita
  ];

  meta = with lib; {
    homepage = "https://gitlab.gnome.org/World/AudioSharing";
    description = "Audio sharing application";
    maintainers = with maintainers; [ linsui ];
    license = licenses.gpl3Plus;
    platforms = platforms.linux;
  };
}

