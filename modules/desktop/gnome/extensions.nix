{ inputs, pkgs, ... }:

with inputs.home-manager.lib.hm.gvariant;

{
  environment.systemPackages = with pkgs.gnomeExtensions; [
    appindicator
    gesture-improvements
    hide-top-bar
    night-theme-switcher
    pop-shell
  ];

  programs.kdeconnect = {
    enable = true;
    package = pkgs.gnomeExtensions.gsconnect;
  };

  hm.dconf.settings = {
    "org/gnome/shell" = {
      disabled-extensions = [
        "screenshot-window-sizer@gnome-shell-extensions.gcampax.github.com"
        "places-menu@gnome-shell-extensions.gcampax.github.com"
        "windowsNavigator@gnome-shell-extensions.gcampax.github.com"
        "simulate-switching-workspaces-on-active-monitor@micheledaros.com"
        "window-list@gnome-shell-extensions.gcampax.github.com"
        "workspace-indicator@gnome-shell-extensions.gcampax.github.com"
        "native-window-placement@gnome-shell-extensions.gcampax.github.com"
      ];
      enabled-extensions = [
        "pop-shell@system76.com"
        "Hide_Activities@shay.shayel.org"
        "drive-menu@gnome-shell-extensions.gcampax.github.com"
        "nightthemeswitcher@romainvigier.fr"
        "launch-new-instance@gnome-shell-extensions.gcampax.github.com"
        "gestureImprovements@gestures"
        "paperwm@hedning:matrix.org"
        "hidetopbar@mathieu.bidon.ca"
        "gsconnect@andyholmes.github.io"
        "trayIconsReloaded@selfmade.pl"
      ];
    };

    "org/gnome/shell/extensions/gestureImprovements" = {
      enable-forward-back-gesture = true;
    };

    "org/gnome/shell/extensions/hidetopbar" = {
      animation-time-overview = 0.4;
      enable-active-window = false;
      enable-intellihide = false;
      hot-corner = false;
      mouse-sensitive = false;
      mouse-sensitive-fullscreen-window = false;
      show-in-overview = true;
    };

    "org/gnome/shell/extensions/nightthemeswitcher/gtk-variants" = {
      night = "Adwaita-dark";
    };

    "org/gnome/shell/extensions/nightthemeswitcher/time" = {
      always-enable-ondemand = true;
      manual-time-source = true;
      ondemand-button-placement = "panel";
      nightthemeswitcher-ondemand-keybinding = [ "<Shift><Super>t" ];
      time-source = "ondemand";
      transition = true;
    };

    "org/gnome/shell/extensions/pop-shell" = {
      active-hint = false;
      gap-inner = mkUint32 0;
      gap-outer = mkUint32 0;
      show-skip-taskbar = false;
      show-title = false;
      smart-gaps = true;
      tile-by-default = true;
    };

    "org/gnome/shell/extensions/window-list" = {
      display-all-workspaces = false;
    };
  };
}
