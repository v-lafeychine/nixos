{ pkgs, ... }:

let
  wofi = pkgs.writeShellScriptBin "rofi" "exec ${pkgs.wofi}/bin/wofi -G $@";
  wtype = pkgs.writeShellScriptBin "xdotool" ''
    if pgrep -x "ydotoold" >/dev/null; then
      printf "%s\n" "ydotool daemon already running"
    else
      printf "%s\n" "starting daemon"
      ydotoold &
    fi

    if [[ $2 == "Tab" ]]; then
      set -- "''$1" 15:1 15:0
    fi

    if [[ $6 == "-" ]]; then
      set -- "''${@:1:5}" "/dev/stdin"
    fi

    ydotool $@
  '';
in {
  user.packages = with pkgs;
    [
      (stdenv.mkDerivation rec {
        pname = "rofi-pass";
        version = "20230707";

        src = fetchFromGitHub {
          owner = "carnager";
          repo = "rofi-pass";
          rev = "e77cbdbe0e885f0b1daba3a0b6bae793cc2b1ba3";
          sha256 = "sha256-zmNuFE+++tf4pKTXSTc7s8R9rvI+XwgWl8mCEPaaIRM=";
        };

        nativeBuildInputs = [ makeWrapper ];

        dontBuild = true;

        installPhase = ''
          mkdir -p $out/bin
          cp -a rofi-pass $out/bin/rofi-pass
          mkdir -p $out/share/doc/rofi-pass/
          cp -a config.example $out/share/doc/rofi-pass/config.example
        '';

        wrapperPath = with lib;
          makeBinPath [
            coreutils
            findutils
            gawk
            gnugrep
            gnused
            libnotify
            (pass.withExtensions (exts: [ exts.pass-otp ]))
            procps
            pwgen
            util-linux
	          wl-clipboard
	          wofi
            wtype
            ydotool
          ];

        fixupPhase = ''
          patchShebangs $out/bin
          wrapProgram $out/bin/rofi-pass \
	          --set ROFI_PASS_BACKEND xdotool \
	          --set ROFI_PASS_CLIPBOARD_BACKEND wl-clipboard \
            --set PATH "${wrapperPath}"
        '';
      })
    ];
}
