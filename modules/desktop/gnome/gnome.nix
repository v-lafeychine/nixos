{ pkgs, ... }:

{
  services = {
    gnome.core-utilities.enable = false;

    xserver = {
      enable = true;

      displayManager.gdm.enable = true;
      desktopManager.gnome.enable = true;

      excludePackages = [ pkgs.xterm ];
    };
  };

  user.packages = with pkgs.gnome; [
    pkgs.blackbox-terminal
    gnome-calculator
    nautilus
  ];
}
