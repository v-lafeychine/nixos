{ pkgs, ... }:

{
  fonts.fonts = with pkgs; [ lmodern fira fira-mono ];

  user.packages = with pkgs; [
    inkscape
    libreoffice
    obs-studio
    pavucontrol
    wl-clipboard
  ];
}
