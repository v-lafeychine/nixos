{ pkgs, ... }:

{
  hm.dconf.settings = { "org/gnome/evolution/mail" = { mark-seen = false; }; };

  user.packages = with pkgs; [ evolution protonmail-bridge ];

  systemd.user.services.protonmail-bridge = {
    enable = true;

    description = "Protonmail Bridge";
    script = "${pkgs.protonmail-bridge}/bin/protonmail-bridge --noninteractive";

    path = [ pkgs.gnome.gnome-keyring ];

    wantedBy = [ "graphical-session.target" ];
    partOf = [ "graphical-session.target" ];
  };
}
