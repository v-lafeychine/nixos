{ pkgs, ... }:

{
  user.packages = with pkgs; [
    caprine-bin
    discord
    element-desktop
    mattermost-desktop
    signal-desktop
  ];
}
