{ pkgs, ... }:

{
  user.packages = with pkgs; [ evince spotify warp ];
}
