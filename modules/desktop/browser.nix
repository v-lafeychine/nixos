{ pkgs, ... }:

{
  hm.programs.firefox = {
    enable = true;

    package = pkgs.firefox-wayland;
  };
}
