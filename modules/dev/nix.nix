{ pkgs, ... }:

{
  user.packages = with pkgs; [ nixfmt ];
}
