{ pkgs, ... }:

{
  user.packages = with pkgs; [
    bear
    clang
    clang-tools
    cmake
    gcc
    gdb
    gnumake
  ];
}
