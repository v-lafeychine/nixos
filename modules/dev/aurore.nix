{ pkgs, ... }:

{
  programs.wireshark = {
    enable = true;
    package = pkgs.wireshark;
  };

  user.packages = with pkgs; [
    apache-directory-studio
    ansible
    dig
    iperf3
    pkg-config
    nftables
    tcpdump
    openssl
    sshpass
  ];
}
