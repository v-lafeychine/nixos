{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    gnuplot

    # For Copilot
    nodejs
  ];
}
