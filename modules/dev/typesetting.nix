{ pkgs, ... }:

{
  user.packages = with pkgs; [
    python3Packages.pygments
    texlive.combined.scheme-full
    typst
  ];
}
