{ pkgs, ... }:

{
  boot.extraModprobeConfig = ''
    options kvm_intel nested=1
    options kvm_intel emulate_invalid_guest_state=0
    options kvm ignore_msrs=1
  '';

  user.packages = [ pkgs.gnome.gnome-boxes ];

  virtualisation.docker = {
    enable = true;
    enableOnBoot = false;
  };

  virtualisation.libvirtd = {
    enable = true;
    onBoot = "ignore";
  };
}
