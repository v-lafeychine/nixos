{ pkgs, ... }:

{
  user.packages = with pkgs; [ metals sbt scala scalafmt ];
}
