{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    nodePackages.pyright
    python3
    python3Packages.black
    python3Packages.isort
    python3Packages.mypy
    python3Packages.pyflakes
    python3Packages.pytest
  ];
}
