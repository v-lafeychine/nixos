(setq doom-theme 'doom-gruvbox-light)

(defun set-theme-from-dbus-value (value)
  "Set the appropiate theme according to the color-scheme setting value."
  (if (equal value '1)
      (load-theme 'doom-gruvbox)
    (load-theme 'doom-gruvbox-light)))

(defun color-scheme-changed (path var value)
  "DBus handler to detect when the color-scheme has changed."
  (when (and (string-equal path "org.freedesktop.appearance")
             (string-equal var "color-scheme"))
    (set-theme-from-dbus-value (car value))))

(use-package! dbus
  :config

  ;; Register for future changes
  (dbus-register-signal
   :session "org.freedesktop.portal.Desktop"
   "/org/freedesktop/portal/desktop" "org.freedesktop.portal.Settings"
   "SettingChanged"
   #'color-scheme-changed)

  ;; Request the current color-scheme
  (dbus-call-method-asynchronously
   :session "org.freedesktop.portal.Desktop"
   "/org/freedesktop/portal/desktop" "org.freedesktop.portal.Settings"
   "Read"
   (lambda (value) (set-theme-from-dbus-value (caar value)))
   "org.freedesktop.appearance"
   "color-scheme"
   ))
