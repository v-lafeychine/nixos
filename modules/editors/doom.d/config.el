;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

(setq user-full-name "Vincent Lafeychine"
      user-mail-address "vincent.lafeychine@proton.me")

(setq org-directory "~/org/")

(setq display-line-numbers-type t)

(setq undo-limit 80000000
      evil-want-fine-undo t
      auto-save-default t)

(setq! evil-toggle-key "C-M-z")


(setq org-latex-pdf-process '("latexmk -f -pdf -%latex -shell-escape -interaction=nonstopmode -output-directory=%o %f"))

(setq org-latex-listings 'minted)
(setq org-latex-minted-options
      '(("escapeinside" "♯♯") ("mathescape")))


(load! "+copilot.el")
(load! "+coq.el")
(load! "+theme.el")

(use-package! typst-mode)

;(after! lsp-mode
;  (add-to-list 'lsp-language-id-configuration '(coq-mode . "coq"))
;  (lsp-register-client
;   (make-lsp-client :new-connection (lsp-stdio-connection "coq-lsp")
;                    :activation-fn (lsp-activate-on "coq")
;                    :server-id 'coq-lsp)))
