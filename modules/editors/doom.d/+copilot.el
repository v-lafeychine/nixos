(defun my-tab ()
  (interactive)
  (or (copilot-accept-completion)
      (indent-for-tab-command)))

(use-package! copilot
  :hook (prog-mode . copilot-mode)
  :bind (
	 ; :map company-active-map
         ;      ("TAB" . 'my-tab)
         ;      ("C-TAB" . 'copilot-next-completion)
         ;      ("C-S-TAB" . 'copilot-previous-completion)

         :map company-mode-map
              ("TAB" . 'my-tab)
              ("C-TAB" . 'copilot-next-completion)
              ("C-S-TAB" . 'copilot-previous-completion)))
