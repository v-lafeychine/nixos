{ ... }:

{
  hm.programs.git = {
    enable = true;

    aliases = { adog = "log --abbrev --decorate --oneline --graph"; };

    difftastic = {
      enable = true;
      display = "inline";
    };

    extraConfig = { 
      github.token = "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";
      init.defaultBranch = "main";
      pull.rebase = true; 
    };

    ignores = [ "result" ];

    lfs.enable = true;

    signing = {
      key = null;
      signByDefault = true;
    };

    userName = "Vincent Lafeychine";
    userEmail = "vincent.lafeychine@proton.me";
  };
}
