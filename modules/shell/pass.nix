{ pkgs, ... }:

{
  hm.programs.password-store = {
    enable = true;
    package = pkgs.pass.withExtensions (exts: [ exts.pass-otp ]);
  };

  user.packages = with pkgs; [ zbar ];
}
