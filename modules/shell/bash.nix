{ pkgs, ... }:

{
  hm.programs.bash = {
    enable = true;
    enableVteIntegration = true;

    historyIgnore = [ "fg" ];

    initExtra = ''
      set -o noclobber
      set -o notify

      [ -z "$PS1" ] || function cd {
        builtin cd "$@" && ls
      }
    '';

    sessionVariables = { EDITOR = "${pkgs.vim}/bin/vim"; };

    shellAliases = {
      cp = "cp -i";
      grep = "grep --color";
      mv = "mv -i";
    };

    shellOptions = [ "autocd" "extglob" "globstar" "histappend" ];
  };

  hm.programs.atuin = {
    enable = true;
    enableBashIntegration = true;

    flags = [ "--disable-up-arrow" ];
  };
}
