{ pkgs, ... }:

{
  hm.programs.bash = {
    initExtra = ''
      eval "$(${pkgs.starship}/bin/starship init bash)"
    '';
  };

  hm.programs.starship = {
    enable = true;

    settings = {
      format = "$status$all";

      character.format = "> ";

      git_branch.symbol = "";

      nix_shell.format = "via [$name]($style) ";

      shlvl.disabled = false;

      status = {
        disabled = false;
        format = "[\\[$status\\]](yellow) ";
      };

      # Disable every language detection
      aws.disabled = true;
      azure.disabled = true;
      battery.disabled = true;
      buf.disabled = true;
      c.disabled = true;
      cmake.disabled = true;
      cobol.disabled = true;
      conda.disabled = true;
      crystal.disabled = true;
      dart.disabled = true;
      deno.disabled = true;
      dotnet.disabled = true;
      elixir.disabled = true;
      elm.disabled = true;
      golang.disabled = true;
      haskell.disabled = true;
      helm.disabled = true;
      java.disabled = true;
      julia.disabled = true;
      git_commit.disabled = true;
      ocaml.disabled = true;
      perl.disabled = true;
      php.disabled = true;
      package.disabled = true;
      pulumi.disabled = true;
      purescript.disabled = true;
      python.disabled = true;
      rlang.disabled = true;
      red.disabled = true;
      ruby.disabled = true;
      rust.disabled = true;
      scala.disabled = true;
      swift.disabled = true;
      vlang.disabled = true;
      zig.disabled = true;
    };
  };
}
