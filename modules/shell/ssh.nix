{ pkgs, ... }:

let
  remarkableTablet = {
    identityFile = "~/.ssh/id_rsa";
    user = "root";
    extraOptions = {
      HostKeyAlgorithms = "+ssh-rsa";
      PubkeyAcceptedKeyTypes = "+ssh-rsa";
    };
  };
in {
  hm.programs.ssh = {
    enable = true;

    matchBlocks = {
      "remarkable" = { hostname = "192.168.1.5"; } // remarkableTablet;

      "remarkable2" = { hostname = "192.168.1.6"; } // remarkableTablet;

      "remarkable-ssh" = { hostname = "10.11.99.1"; } // remarkableTablet;

      "*.dptinfo !ssh.dptinfo" = {
        hostname = "%h.ens-cachan.fr";
        identityFile = "~/.ssh/id_ed25519";
        proxyJump = "lafeychine@ssh.dptinfo.ens-cachan.fr";
        user = "lafeychine";
      };

      "10.128.* *.auro.re" = {
        identityFile = "~/.ssh/id_ed25519";
        user = "root";
      };

      "10.128.* *.auro.re !passerelle.auro.re" = {
        proxyJump = "passerelle.auro.re";
      };
    };
  };
}
