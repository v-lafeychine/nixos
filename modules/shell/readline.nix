{ ... }:

{
  hm.programs.readline = {
    enable = true;

    includeSystemConfig = true;

    bindings = {
      "\\e[A" = "history-search-backward";
      "\\eOA" = "history-search-backward";
      "\\e[B" = "history-search-forward";
      "\\eOB" = "history-search-backward";
    };

    variables = {
      colored-stats = "on";
      completion-ignore-case = "on";
      completion-prefix-display-length = "3";
      show-all-if-ambiguous = "on";
    };
  };
}
