{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    (aspellWithDicts (dicts: with dicts; [ en en-computers en-science ]))
    bat
    bc
    binutils
    coreutils
    cpufrequtils
    fd
    file
    findutils
    fzf
    killall
    libtool
    lm_sensors
    ltrace
    pandoc
    pciutils
    pdftk
    ripgrep
    traceroute
    tree
    unzip
    usbutils
    wget
    zip
  ];
}
