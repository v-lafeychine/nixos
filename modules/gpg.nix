{ pkgs, ... }:

{
  hm.programs.gpg = {
    enable = true;

    settings = {
      expert = true;
      keyserver = "keyserver.ubuntu.com";
    };
  };

  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };

  services.pcscd.enable = true;
  services.udev.packages = [ pkgs.yubikey-personalization ];

  user.packages = with pkgs; [
    yubico-pam
    yubikey-manager
    yubikey-manager-qt
    yubikey-personalization
    yubikey-personalization-gui
  ];
}
